﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Player's representation in Lobby
public class LobbyPlayer : NetworkLobbyPlayer
{
    [SyncVar]
    public string playerName = "";
    [SyncVar]
    public PlayerColorID color;

    public override void OnStartLocalPlayer()
    {
        SendReadyToBeginMessage();
        base.OnStartLocalPlayer();
    }

    public override void OnClientEnterLobby()
    {
        int idx = NetworkManager.singleton.numPlayers;
        playerName = string.Format("Player {0}", idx);
        color = (PlayerColorID) (idx - 1);
        if (!isLocalPlayer)
            readyToBegin = true;
    }
}
