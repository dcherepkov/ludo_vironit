﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TableTopItem : NetworkBehaviour
{
    //Можно ли двигать? Да, если не на базе и не дома
    [SyncVar]
    public bool movable;
    //На базе ли фигурка
    [SyncVar]
    public bool nested;
    //Цвет фигурки
    public PlayerColorID color;
    //Сколько ей шагать до конца
    [SyncVar]
    public int stepsRemaining;
    //Индекс waypoint'a, в котором она стоит
    //Обновляется при перемещении в новую ячейку
    [SyncVar]
    public int startedFrom;

    private void Awake()
    {
        movable = false;
        nested = true;
    }
    //Обновляет счётчик шагов
    public bool UpdateStepsRemaining(int stepsToMake, bool finals = false)
    {
        //Нельзя шагать, если бросили больше, чем осталось
        bool canMove = stepsRemaining >= stepsToMake && movable;
        if (canMove)
        {
            stepsRemaining -= stepsToMake;
        }

        return canMove;
    }

    //При нажатии на фигурку
    public void OnSelect()
    {
        //Подписчик - контроллер
        NotificationCenter.ItemSelected(this);
    }
}
