﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class Table : NetworkBehaviour
{
    public Waypoint[] points;
    public Dictionary<PlayerColorID, Waypoint> spawnPoints;
    public TableTopItem[] items;

    private void Awake()
    {
        //Find all points on table
        points = FindObjectsOfType<Waypoint>();
        //Filter out non-spawn points
        var spawns = points.Where(p => p.spawnPoint && p.correctColor != PlayerColorID.NONE).ToArray();

        //Packaging to dictionary
        spawnPoints = new Dictionary<PlayerColorID, Waypoint>();        
        foreach (var spawn in spawns)
        {
            spawnPoints.Add(spawn.correctColor, spawn);
        }
        //find items for all players
        items = FindObjectsOfType<TableTopItem>();
    }
}
