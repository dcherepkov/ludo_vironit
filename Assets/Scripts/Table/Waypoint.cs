﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//single point on table
public class Waypoint : MonoBehaviour
{
    //Which player can fully handle this point?
    public PlayerColorID correctColor;
    //Is this point - the first point to start from?
    public bool spawnPoint;
    //Is this point is 'arrow' marked
    public bool arrow;
    //Colored means last points to finish
    public bool colored = false;
}
