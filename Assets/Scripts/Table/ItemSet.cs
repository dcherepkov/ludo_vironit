﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ItemSet : ScriptableObject
{
    public Sprite horseSprite;
	public Sprite knightSprite;
    public Sprite kingSprite;
    public Sprite queenSprite;
}
