﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyManager : NetworkLobbyManager
{
    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        GamePlayer player = gamePlayer.GetComponent<GamePlayer>();
        LobbyPlayer lPlayer = lobbyPlayer.GetComponent<LobbyPlayer>();
        
        player.playerName = lPlayer.playerName;
        player.colorId = lPlayer.color;
        return true;
    }
}
