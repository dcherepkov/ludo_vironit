﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

//Player class
public class GamePlayer : NetworkBehaviour
{
    [SyncVar]
    public string playerName;
    [SyncVar]
    public PlayerColorID colorId;
    //Is this player active, i.e. is this player's turn?
    [SyncVar(hook = "OnActiveStateChanged")]
    public bool active;
    //Controled items (to select and highlight)
    public TableTopItem[] items;

    private void Awake()
    {
        //Add self to controller's list of all players
        GameplayController.players.Add(this);
    }

    public void ChangeStateTo(bool state)
    {
        //Triggering 'active' state and items' hightlight
        active = state;
    }

    //SyncVar hook
    private void OnActiveStateChanged(bool activeState)
    {
        active = activeState;
        //Highlighting each controled item
        items.ToList().ForEach(i => i.transform.localScale = Vector3.one * (active ? 1.5f : 1f));
    }

    [Command]
    //Triggering turn change from client to server
    public void CmdNextPlayer()
    {
        Debug.Log(playerName);
        GameplayController.sharedInstance.NextPlayer();
    }
}

