﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationCenter : MonoBehaviour
{
    public delegate void TabletTopItemSelectedDelegate(TableTopItem item);
    public static TabletTopItemSelectedDelegate OnTableTopItemSelected;

    public static void ItemSelected(TableTopItem selectedItem)
    {
        if(OnTableTopItemSelected != null)
        {
            OnTableTopItemSelected(selectedItem);
        }
    }

    public delegate void ActivePlayerChanged(PlayerColorID colorId);
    public static ActivePlayerChanged OnActivePlayerChanged;

    public static void PlayerChanged(PlayerColorID colorId)
    {
        if(OnActivePlayerChanged != null)
        {
            OnActivePlayerChanged(colorId);
        }
    }
}
