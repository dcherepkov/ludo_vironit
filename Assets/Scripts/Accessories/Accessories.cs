﻿using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Вспомогательный класс
/// </summary>
public class Accessories 
{
    /// <summary>
    /// Generates unique numerical key (room code)
    /// </summary>
    /// <returns>The unique numerical key.</returns>
    /// <param name="maxSize">Key's length</param>
    public static string GetUniqueKey(int maxSize)
    {
        char[] chars = "1234567890".ToCharArray();
        byte[] data = new byte[1];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        data = new byte[maxSize];
        crypto.GetNonZeroBytes(data);

        StringBuilder result = new StringBuilder(maxSize);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length)]);
        }

        return result.ToString();
    }
}
