﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controller to rule lobby scene
public class LobbySceneController : MonoBehaviour
{
    public LobbyManager lobbyManager;

	void Start ()
    {
        lobbyManager = FindObjectOfType<LobbyManager>();
        lobbyManager.StartMatchMaker();
    }

    public void OnConnectPress(int playerCount)
    {
        lobbyManager.maxPlayers = playerCount;
        lobbyManager.minPlayers = playerCount;
        //Find matches with same player count
        lobbyManager.matchMaker.ListMatches(0, 1, "", true, 0, playerCount, (success, extendedInfo, result) =>
        {
            if (result.Count > 0 && success)
            {
                //if found - join
                var info = result[0];
                lobbyManager.matchMaker.JoinMatch(info.networkId, "", "", "", 0, playerCount, lobbyManager.OnMatchJoined);
            }
            else
            {
                //else - create new and wait
                lobbyManager.matchMaker.CreateMatch("", (uint)lobbyManager.maxPlayers, true, "", "", "", 0, playerCount, lobbyManager.OnMatchCreate);
            }
        });
    }

    
}
