﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System;

public enum PlayerColorID
{
    NONE = -1, Yellow, Red, Green, Blue
}

/// <summary>
/// Main controller to rule the game session
/// </summary>
public class GameplayController : NetworkBehaviour
{
    //List of all players
    public static List<GamePlayer> players = new List<GamePlayer>();
    //singleton reference
    public static GameplayController sharedInstance { get; private set; }
    //Color of player who has able to make a move/roll the dice
    [SyncVar(hook = "CurrentChanged")]
    public PlayerColorID currentColorId;
    //Local player's referenced color
    private PlayerColorID localPlayeColor;
    //Gameplay table
    public Table table;

    private void Awake()
    {
        sharedInstance = this;    
    }

    private void Start()
    {
        foreach (var player in players)
        {
            //Assign items by player's color
            player.items = table.items.Where(i => i.color == player.colorId).ToArray();
        }

        localPlayeColor = players.Single(p => p.isLocalPlayer).colorId;
    }

    public void OnRollButtonClick()
    {
        if (currentColorId != localPlayeColor) return;

        if (isServer)
            //directly selecting next player to switch turn to
            NextPlayer();
        else
            //Indirectly calling the same method from client
            players.Single(p => p.colorId == currentColorId).CmdNextPlayer();
    }

    [ServerCallback]
    public void NextPlayer()
    {
        //Selecting next player... draft
        var cID = ((int)(currentColorId + 1)) % NetworkManager.singleton.numPlayers;
        currentColorId = (PlayerColorID) cID;
    }    
    //SyncVar hook
    void CurrentChanged(PlayerColorID cur)
    {
        currentColorId = cur;
        //Changing 'active' state
        players.ForEach(p => p.ChangeStateTo(p.colorId == currentColorId));        
    }
}
